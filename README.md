# README #
### Contents ###
* Problem Statement
* Run/Build/Test Options

### Problem Statement ###

The following matrix represents the cost of flying between Castle Black, Winterfell, Riverrun and King's Landing.
~~~~
    costs =
        [[0, 15, 80, 90],
        [0, 0, 40, 50],
        [0, 0, 0, 70],
        [0, 0, 0, 0]]
~~~~

For example:
Flying from Castle Black to Winterfell costs 15 Silver Stags (<code>costs[0][1]</code>)
<p>
Flying from Castle Black to King's Landing costs 90 Silver Stags (<code>costs[0][3]</code>)
<p>
Flying from Riverrun to King's Landing costs 70 Silver Stags (<code>costs[2][3]</code>)
<p>
You can only fly from north to south, e.g you can fly from Winterfell to King's Landing, but not from King's Landing to Riverrun.
<p>
Write an algorithm to find the cost for each possible flight path from Castle Black to King's Landing and write them to standard output.
The expected end solution should work like this:

~~~~
./bin/list-flight-paths "Castle Black" "Winterfell"
Castle Black -> Winterfell: 15
./bin/list-flight-paths "Castle Black" "Riverrun"
Castle Black -> Winterfell -> Riverrun: 55
Castle Black -> Riverrun: 80
~~~~

### Run/Build/Test Options ###
* Execute Application
<p>
  The easiest way to run the application is running the following command:

~~~~
.\mvnw clean compile exec:java -D"exec.mainClass"="com.love.holidays.task.flights.FlightCostApplicationMain" -D"exec.arguments"="Castle Black,King's Landing"
~~~~
Where command line arguments are passed as comma-separated Strings via -D"exec.arguments" parameter.
~~~~
-D"exec.arguments"="Castle Black,King's Landing"
~~~~

* Run tests
~~~~
.\mvnw test
~~~~
* Build Application
~~~~
.\mvnw clean package
~~~~