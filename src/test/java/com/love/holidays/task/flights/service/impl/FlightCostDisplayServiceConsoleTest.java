package com.love.holidays.task.flights.service.impl;

import com.love.holidays.task.flights.dto.FlightCost;
import com.love.holidays.task.flights.service.FlightCostDisplayService;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FlightCostDisplayServiceConsoleTest {

    @Test
    public void testPrintWithNoRoutes() throws IOException {
        //given
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

        FlightCostDisplayService flightCostDisplayService = new
                FlightCostDisplayServiceConsole(Map.of(), outputStreamCaptor);
        //when
        flightCostDisplayService.print(List.of());
        //then
        assertEquals("No available flights found", outputStreamCaptor.toString());
    }

    @Test
    public void testPrintWithOneRoute() throws IOException {
        //given
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

        FlightCostDisplayService flightCostDisplayService = new
                FlightCostDisplayServiceConsole(Map.of(1, "cityA", 2, "cityB"), outputStreamCaptor);
        //when
        flightCostDisplayService.print(
                List.of(new FlightCost(List.of(1, 2), 50))
        );
        //then
        assertEquals("cityA -> cityB : 50", outputStreamCaptor.toString());
    }

    @Test
    public void testPrintWithTwoRoutes() throws IOException {
        //given
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

        FlightCostDisplayService flightCostDisplayService = new
                FlightCostDisplayServiceConsole(Map.of(1, "cityA", 2, "cityB", 3, "cityC"), outputStreamCaptor);
        //when
        flightCostDisplayService.print(
                List.of(
                        new FlightCost(List.of(1, 2, 3), 50),
                        new FlightCost(List.of(1, 3), 60)
                ));
        //then
        assertEquals("cityA -> cityB -> cityC : 50\ncityA -> cityC : 60", outputStreamCaptor.toString());
    }
}