package com.love.holidays.task.flights.service.validator.impl;

import com.love.holidays.task.flights.exception.FlightCostApplicationException;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class TwoCitiesCommandLineArgumentsValidatorTest {

    @Test
    public void testValidateWhenArgumentsAreNullThrowException(){
        Map<String, Integer> cityMap = Map.of();
        TwoCitiesCommandLineArgumentsValidator validator = new TwoCitiesCommandLineArgumentsValidator(cityMap);
        //when
        //then
        FlightCostApplicationException exception = assertThrows(FlightCostApplicationException.class, () -> validator.validate(null));
        assertEquals("Input city parameters can not be empty.", exception.getMessage());
    }

    @Test
    public void testValidateWhenArgumentsAreEmptyThrowException(){
        Map<String, Integer> cityMap = Map.of();
        TwoCitiesCommandLineArgumentsValidator validator = new TwoCitiesCommandLineArgumentsValidator(cityMap);
        //when
        //then
        FlightCostApplicationException exception = assertThrows(FlightCostApplicationException.class, () -> validator.validate(new String [0]));
        assertEquals("Input city parameters can not be empty.", exception.getMessage());
    }

    @Test
    public void testValidateWhenOneArgumentProvidedThrowException(){
        Map<String, Integer> cityMap = Map.of();
        TwoCitiesCommandLineArgumentsValidator validator = new TwoCitiesCommandLineArgumentsValidator(cityMap);
        //when
        //then
        FlightCostApplicationException exception = assertThrows(FlightCostApplicationException.class, () -> validator.validate(new String[] {"city1"}));
        assertEquals("Only one city provided. Expected two.", exception.getMessage());
    }

    @Test
    public void testValidateWhenThreeArgumentProvidedThrowException(){
        Map<String, Integer> cityMap = Map.of();
        TwoCitiesCommandLineArgumentsValidator validator = new TwoCitiesCommandLineArgumentsValidator(cityMap);
        //when
        //then
        FlightCostApplicationException exception = assertThrows(FlightCostApplicationException.class, () -> validator.validate(new String[] {"city1", "city2", "city3"}));
        assertEquals("Too many cities provided. Expected two.", exception.getMessage());
    }

    @Test
    public void testValidateWhenFirstCityIsUnknownThrowException(){
        Map<String, Integer> cityMap = Map.of( "city2", 0);
        TwoCitiesCommandLineArgumentsValidator validator = new TwoCitiesCommandLineArgumentsValidator(cityMap);
        //when
        //then
        FlightCostApplicationException exception = assertThrows(FlightCostApplicationException.class, () -> validator.validate(new String[] {"city1", "city2"}));
        assertEquals("City \"city1\" is not a known city.", exception.getMessage());
    }

    @Test
    public void testValidateWhenSecondCityIsUnknownThrowException(){
        Map<String, Integer> cityMap = Map.of( "city1", 0);
        TwoCitiesCommandLineArgumentsValidator validator = new TwoCitiesCommandLineArgumentsValidator(cityMap);
        //when
        //then
        FlightCostApplicationException exception = assertThrows(FlightCostApplicationException.class, () -> validator.validate(new String[] {"city1", "city2"}));
        assertEquals("City \"city2\" is not a known city.", exception.getMessage());
    }

    @Test
    public void testValidateWhenFlyToSameCityThrowException(){
        Map<String, Integer> cityMap = Map.of( "city1", 0, "city2", 1);
        TwoCitiesCommandLineArgumentsValidator validator = new TwoCitiesCommandLineArgumentsValidator(cityMap);
        //when
        //then
        FlightCostApplicationException exception = assertThrows(FlightCostApplicationException.class, () -> validator.validate(new String[] {"city1", "city1"}));
        assertEquals("Destination city can not be same as starting city.", exception.getMessage());
    }

    @Test
    public void testValidateWhenFlyToSouthThrowException(){
        Map<String, Integer> cityMap = Map.of( "city1", 1, "city2", 0);
        TwoCitiesCommandLineArgumentsValidator validator = new TwoCitiesCommandLineArgumentsValidator(cityMap);
        //when
        //then
        FlightCostApplicationException exception = assertThrows(FlightCostApplicationException.class, () -> validator.validate(new String[] {"city1", "city2"}));
        assertEquals("Can not fly from south to north.", exception.getMessage());
    }

    @Test
    public void testValidateWhenValidInputDoNothing(){
        Map<String, Integer> cityMap = Map.of( "city1", 0, "city2", 1);
        TwoCitiesCommandLineArgumentsValidator validator = new TwoCitiesCommandLineArgumentsValidator(cityMap);
        //when
        //then
        validator.validate(new String[] {"city1", "city2"});
    }
}