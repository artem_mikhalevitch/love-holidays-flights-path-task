package com.love.holidays.task.flights.service.impl;

import com.love.holidays.task.flights.dto.FlightCost;
import com.love.holidays.task.flights.service.FlightCostFinderService;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FlightsFinderServiceDFSTest {

    @Test
    public void testFindFlightsWhenFlightToSelfReturnEmpty() {
        //given
        Map<Integer, List<Integer>> graph =
                Map.of(
                        0, List.of(0, 1),
                        1, List.of(0, 0)
                );
        FlightCostFinderService flightCostFinderService =
                new FlightCostFinderServiceDFS(graph);
        //when
        List<FlightCost> flights = flightCostFinderService.findFlights(0, 0);
        //then
        assertEquals(List.of(), flights);
    }

    @Test
    public void testFindFlightsWhenTwoCitiesAndNoFlightsReturnEmpty() {
        //given
        Map<Integer, List<Integer>> graph =
                Map.of(
                        0, List.of(0, 0),
                        1, List.of(0, 0)
                );
        FlightCostFinderService flightCostFinderService =
                new FlightCostFinderServiceDFS(graph);
        //when
        List<FlightCost> flights = flightCostFinderService.findFlights(0, 1);
        //then
        assertEquals(List.of(), flights);
    }

    @Test
    public void testFindFlightsWhenTwoCitiesAndOneFlightReturnOne() {
        //given
        Map<Integer, List<Integer>> graph =
                Map.of(
                        0, List.of(0, 100),
                        1, List.of(0, 0)
                );
        FlightCostFinderService flightCostFinderService =
                new FlightCostFinderServiceDFS(graph);
        //when
        List<FlightCost> flights = flightCostFinderService.findFlights(0, 1);
        //then
        assertEquals(List.of(
                new FlightCost(List.of(0, 1), 100)
        ), flights);
    }

    @Test
    public void testFindFlightsWhenThreeCitiesAndTwoFlightReturnTwo() {
        //given
        Map<Integer, List<Integer>> graph =
                Map.of(
                        0, List.of(0, 10, 100),
                        1, List.of(0, 0, 200),
                        2, List.of(0, 0, 0)
                );
        FlightCostFinderService flightCostFinderService =
                new FlightCostFinderServiceDFS(graph);
        //when
        List<FlightCost> flights = flightCostFinderService.findFlights(0, 2);
        //then
        assertEquals(List.of(
                new FlightCost(List.of(0, 1, 2), 210),
                new FlightCost(List.of(0, 2), 100)
        ), flights);
    }

    @Test
    public void testFindFlightsWhenThreeCitiesAndOneRouteFromFirstReturnOne() {
        //given
        Map<Integer, List<Integer>> graph =
                Map.of(
                        0, List.of(0, 0, 100),
                        1, List.of(0, 0, 0),
                        2, List.of(0, 0, 0)
                );
        FlightCostFinderService flightCostFinderService =
                new FlightCostFinderServiceDFS(graph);
        //when
        List<FlightCost> flights = flightCostFinderService.findFlights(0, 2);
        //then
        assertEquals(List.of(
                new FlightCost(List.of(0, 2), 100)
        ), flights);
    }

    @Test
    public void testFindFlightsWhenThreeCitiesAndOneRouteFromSecondReturnOne() {
        //given
        Map<Integer, List<Integer>> graph =
                Map.of(
                        0, List.of(0, 10, 0),
                        1, List.of(0, 0, 100),
                        2, List.of(0, 0, 0)
                );
        FlightCostFinderService flightCostFinderService =
                new FlightCostFinderServiceDFS(graph);
        //when
        List<FlightCost> flights = flightCostFinderService.findFlights(0, 2);
        //then
        assertEquals(List.of(
                new FlightCost(List.of(0, 1, 2), 110)
        ), flights);
    }
}