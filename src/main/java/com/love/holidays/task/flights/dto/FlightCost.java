package com.love.holidays.task.flights.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class FlightCost implements Cloneable {

    /**
     * The list of city indexes that have been
     * visited.
     */
    private List<Integer> route;

    /**
     * The accumulative price of flying
     * through @{route} cities
     */
    private Integer price;

    /**
     * Creates a new object
     * with empty @{code route} and zero @{price}
     */
    public FlightCost() {
        route = new ArrayList<>();
        price = 0;
    }

    /**
     * Adds given @{code city} to @{code route}
     * and accumulates route price with @{code price}
     *
     * @param city city to add to route
     * @param price cost of recent flight to city
     */
    public void addRoute(Integer city, Integer price) {
        route.add(city);
        this.price += price;
    }

    @Override
    public FlightCost clone() {
        FlightCost clone = new FlightCost();
        clone.route.addAll(route);
        clone.price = price;

        return clone;
    }
}