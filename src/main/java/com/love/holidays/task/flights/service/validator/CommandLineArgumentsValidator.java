package com.love.holidays.task.flights.service.validator;

import com.love.holidays.task.flights.exception.FlightCostApplicationException;

/**
 * Provides validation options to
 * validate Command Line arguments @{code String[]args}
 */
public interface CommandLineArgumentsValidator {

    /**
     * Validates @{args} to be expected
     * command line options
     *
     * @param args command line options
     * @throws FlightCostApplicationException if validation fails
     */
    void validate(String[] args);
}
