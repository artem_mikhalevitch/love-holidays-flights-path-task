package com.love.holidays.task.flights;

import com.love.holidays.task.flights.dto.FlightCost;
import com.love.holidays.task.flights.exception.FlightCostApplicationException;
import com.love.holidays.task.flights.service.FlightCostDisplayService;
import com.love.holidays.task.flights.service.FlightCostFinderService;
import com.love.holidays.task.flights.service.impl.FlightCostDisplayServiceConsole;
import com.love.holidays.task.flights.service.impl.FlightCostFinderServiceDFS;
import com.love.holidays.task.flights.service.validator.CommandLineArgumentsValidator;
import com.love.holidays.task.flights.service.validator.impl.TwoCitiesCommandLineArgumentsValidator;

import java.util.List;
import java.util.Map;

public class FlightCostApplicationMain {

    private static final Map<Integer, List<Integer>> graph =
            Map.of(
                    0, List.of(0, 15, 80, 90),
                    1, List.of(0, 0, 40, 50),
                    2, List.of(0, 0, 0, 70),
                    3, List.of(0, 0, 0, 0)
            );

    private static final Map<Integer, String> cityIndexToNameMap =
            Map.of(0, "Castle Black",
                    1, "Winterfell",
                    2, "Riverrun",
                    3, "King's Landing");

    private static final Map<String, Integer> cityNameToIndexMap =
            Map.of("Castle Black", 0,
                    "Winterfell", 1,
                    "Riverrun", 2,
                    "King's Landing", 3);


    public static void main(String[] args) {
        try {
            validateCommandLineArguments(args);

            int cityFrom = cityNameToIndexMap.get(args[0]);
            int cityTo = cityNameToIndexMap.get(args[1]);

            printFlights(findFlights(cityFrom, cityTo));

            System.exit(0);
        } catch (FlightCostApplicationException e) {
            System.err.print(e.getMessage());
            System.exit(1);
        }
    }

    private static void validateCommandLineArguments(String[] args) {
        CommandLineArgumentsValidator validator = new TwoCitiesCommandLineArgumentsValidator(cityNameToIndexMap);
        validator.validate(args);
    }

    private static List<FlightCost> findFlights(int cityFrom, int cityTo) {
        FlightCostFinderService flightCostFinderService = new FlightCostFinderServiceDFS(graph);
        return flightCostFinderService.findFlights(cityFrom, cityTo);
    }

    private static void printFlights(List<FlightCost> flights) {
        FlightCostDisplayService flightCostDisplayService = new FlightCostDisplayServiceConsole(cityIndexToNameMap, System.out);
        flightCostDisplayService.print(flights);
    }
}
