package com.love.holidays.task.flights.service;

import com.love.holidays.task.flights.dto.FlightCost;

import java.util.List;

/**
 * Finds all routes and their costs
 * between cities
 *
 * @see FlightCost
 */
public interface FlightCostFinderService {

    /**
     * Finds all available routes and their costs
     * from city with index {@code from} to city with index {@code to}.
     *
     * @param from the index of city to find flights
     * @param to   the index of flight's destination city
     * @return the {@code List} of flight costs
     * @see FlightCost
     */
    List<FlightCost> findFlights(Integer from, Integer to);
}
