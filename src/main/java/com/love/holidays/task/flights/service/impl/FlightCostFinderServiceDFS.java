package com.love.holidays.task.flights.service.impl;

import com.love.holidays.task.flights.dto.FlightCost;
import com.love.holidays.task.flights.service.FlightCostFinderService;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Finds all routes and their costs
 *
 * @see FlightCost
 */
@RequiredArgsConstructor
public class FlightCostFinderServiceDFS implements FlightCostFinderService {

    private final Map<Integer, List<Integer>> graph;

    /**
     * Finds all available routes and their costs
     * from city with index {@code from} to city with index {@code to}.
     * Based on Depth-First-Search algorithm
     * as initial data is represented as acyclic graph.
     *
     * @param from the index of city to find flights
     * @param to   the index of flight's destination city
     * @return the {@code List} of flight costs
     * @see FlightCost
     */
    @Override
    public List<FlightCost> findFlights(Integer from, Integer to) {

        if (from.equals(to)) {
            return List.of();
        }

        FlightCost path = new FlightCost();
        path.addRoute(from, 0);

        return this.dfs(from, to, path);
    }

    /**
     * Recursive Depth-First-Search based algorithm to
     * find all available flights from current city @{code currentCity}
     * with known previous route @{code path}
     *
     * @param currentCity the index of city to find flights
     * @param to   the index of flight's destination city
     * @param path the previous path to @{code currentCity}
     * @return the {@code List} of flight costs
     * @see FlightCost
     */
    private List<FlightCost> dfs(Integer currentCity, Integer to, FlightCost path) {
        List<FlightCost> results = new ArrayList<>();

        if (currentCity.equals(to)) {
            results.add(path);
            return results;
        }

        List<Integer> costsFromCurrentCity = graph.get(currentCity);

        for (int nextCity = 0; nextCity < costsFromCurrentCity.size(); nextCity++) {
            int costToNextCity = costsFromCurrentCity.get(nextCity);

            if (costToNextCity <= 0) {
                continue;
            }

            FlightCost candidate = path.clone();
            candidate.addRoute(nextCity, costToNextCity);

            results.addAll(dfs(nextCity, to, candidate));
        }

        return results;
    }
}
