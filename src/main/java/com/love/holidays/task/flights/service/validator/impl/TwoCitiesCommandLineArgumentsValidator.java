package com.love.holidays.task.flights.service.validator.impl;

import com.love.holidays.task.flights.exception.FlightCostApplicationException;
import com.love.holidays.task.flights.service.validator.CommandLineArgumentsValidator;
import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.Objects;


/**
 * {@inheritDoc}
 * Validates the input data to be expected two
 * arguments with values of know cities with
 * valid flights between them.
 */
@RequiredArgsConstructor
public class TwoCitiesCommandLineArgumentsValidator implements CommandLineArgumentsValidator {

    private final Map<String, Integer> cityNameToIndexMap;

    /**
     * {@inheritDoc}
     * <p>
     * Validates arguments size, cities and flights allowance between them
     */
    @Override
    public void validate(String[] args) {
        if (Objects.isNull(args) || args.length == 0) {
            throw new FlightCostApplicationException("Input city parameters can not be empty.");
        }
        if (args.length == 1) {
            throw new FlightCostApplicationException("Only one city provided. Expected two.");
        }
        if (args.length > 2) {
            throw new FlightCostApplicationException("Too many cities provided. Expected two.");
        }
        validateCities(args[0], args[1]);
    }

    /**
     * Validates cities and flights allowance between them
     *
     * @param cityFrom city name flight starts with
     * @param cityTo   city name flight ends with
     */
    private void validateCities(String cityFrom, String cityTo) {
        validateCity(cityFrom);
        validateCity(cityTo);

        int cityFromIndex = cityNameToIndexMap.get(cityFrom);
        int cityToIndex = cityNameToIndexMap.get(cityTo);

        if (cityToIndex == cityFromIndex) {
            throw new FlightCostApplicationException("Destination city can not be same as starting city.");
        }

        if (cityFromIndex > cityToIndex) {
            throw new FlightCostApplicationException("Can not fly from south to north.");
        }
    }

    /**
     * Validates city to be a known city
     *
     * @param city city name flight starts with
     */
    private void validateCity(String city) {
        if (!cityNameToIndexMap.containsKey(city)) {
            throw new FlightCostApplicationException("City \"" + city + "\" is not a known city.");
        }
    }
}
