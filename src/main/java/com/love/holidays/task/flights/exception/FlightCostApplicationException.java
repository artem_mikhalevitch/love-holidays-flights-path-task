package com.love.holidays.task.flights.exception;

public class FlightCostApplicationException extends RuntimeException {

    public FlightCostApplicationException(String message) {
        super(message);
    }
}
