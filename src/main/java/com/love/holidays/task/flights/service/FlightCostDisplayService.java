package com.love.holidays.task.flights.service;

import com.love.holidays.task.flights.dto.FlightCost;

import java.util.List;

/**
 * {@code FlightCostDisplayService} provides functionality
 * to display @{code List<FlightCost>} on screen using
 * standard streams (stdout).
 *
 * @see FlightCost
 */
public interface FlightCostDisplayService {

    /**
     * Prints the given Flight costs using standard streams
     * in defined format
     *
     * @param flightCosts the desired {@code List} of flight costs
     * @see FlightCost
     */
    void print(List<FlightCost> flightCosts);
}
