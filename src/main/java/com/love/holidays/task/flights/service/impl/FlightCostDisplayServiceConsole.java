package com.love.holidays.task.flights.service.impl;

import com.love.holidays.task.flights.dto.FlightCost;
import com.love.holidays.task.flights.exception.FlightCostApplicationException;
import com.love.holidays.task.flights.service.FlightCostDisplayService;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * {@code FlightCostDisplayService} provides functionality
 * to display @{code List<FlightCost>} on screen using
 * provided standard streams (stdout).
 *
 * @see FlightCost
 */
@RequiredArgsConstructor
public class FlightCostDisplayServiceConsole implements FlightCostDisplayService {

    private final Map<Integer, String> cityIndexToNameMap;
    private final OutputStream outputStream;

    /**
     * Prints the given Flight costs using standard streams
     * in a format
     * city 1 -> city 2 -> city 3 : price1 \n
     * city 1 -> city 3 : price2 \n
     *
     * Prints "No available flights found" if no flight costs provided.
     *
     * @param flightCosts the desired {@code List} of flight costs
     * @see FlightCost
     */
    public void print(List<FlightCost> flightCosts) {
        try {

            if (flightCosts.isEmpty()) {
                outputStream.write("No available flights found".getBytes());
            }

            String result = flightCosts.stream()
                    .map(flightPathDto -> {
                        String joinedPath = flightPathDto.getRoute().stream().
                                map(cityIndexToNameMap::get)
                                .collect(Collectors.joining(" -> "));
                        return MessageFormat.format("{0} : {1}", joinedPath, flightPathDto.getPrice());
                    }).collect(Collectors.joining("\n"));


            outputStream.write(result.getBytes());
        } catch (IOException e) {
            throw new FlightCostApplicationException("Unable to access output stream");
        }
    }
}
